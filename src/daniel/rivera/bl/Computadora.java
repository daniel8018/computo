package daniel.rivera.bl;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Computadora {
    private String serie, marca;
    private Empleado empleadoResponsable;

    public Computadora() {
    }

    /**
     * Constructor que recibe todos los parámetros de computadora y los Inicializa
     * @param serie
     * @param marca
     * @param empleadoResponsable
     */
    public Computadora(String serie, String marca, Empleado empleadoResponsable) {
        this.serie = serie;
        this.marca = marca;
        this.empleadoResponsable = empleadoResponsable;
    }

    /**
     * Constructor que 2 parámetros de serie y marca y los Inicializa
     * @param serie
     * @param marca
     */
    public Computadora(String serie, String marca) {
        this.serie = serie;
        this.marca = marca;
    }

    /**
     * Metodo utilizado para modificar el atributo privado responsable de la clase Computadora
     * @param pempo El ejemplo de empleado asignado
     */
    public void setResponsable(Empleado pempo) {
        empleadoResponsable = pempo;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada serie de la clase Computadora
     * @return la variable de retorno simboliza la serie
     */
    public String getSerie() {
        return serie;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Serie de la clase Computadora
     * @param serie Variable que simboliza la serie de la clase Computadora
     */
    public void setSerie(String serie) {
        this.serie = serie;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Marca de la clase Computadora
     * @return la variable de retorno simboliza la marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Marca de la clase Computadora
     * @param marca Variable que simboliza la marca de la clase Computadora
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Empleado responsable de la clase Computadora
     * @return la variable de retorno simboliza el empleado responsable
     */
    public Empleado getEmpleadoResponsable() {
        return empleadoResponsable;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Empleado de la clase Computadora
     * @param empleadoResponsable Variable que simboliza el empleador-resposable de la clase Computadora
     */
    public void setEmpleadoResponsable(Empleado empleadoResponsable) {
        this.empleadoResponsable = empleadoResponsable;
    }

     /**
          * Metodo utilizado para imprimir todos los atributos de la clase computadora en un unico String
          * @return la variable de retorno simboliza todos los valores de la clase computadora en unico string
          */
    public String toCadena() {
        return "Computadora{" +
                "serie='" + serie + '\'' +
                ", marca='" + marca + '\'' +
                '}';
    }

 /**
      * Metodo utilizado para imprimir todos los atributos de la clase Computadora en un unico String
      * @return la variable de retorno simboliza todos los valores de la clase Computadora en unico string
      */
    @Override
    public String toString() {
        return "Computadora{" +
                "serie='" + serie + '\'' +
                ", marca='" + marca + '\'' +
                ", empleadoResponsable=" + empleadoResponsable +
                '}';
    }
}
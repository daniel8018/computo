package daniel.rivera.bl;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Empleado {
    private String nombre, cedula;
    private Computadora compu;

    /**
     * Constructor que recibe todos 2 parametros de Empleado y los Inicializa
     * @param nombre
     * @param cedula
     */
    public Empleado(String nombre, String cedula) {
        this.nombre = nombre;
        this.cedula = cedula;
    }

    /**
     * Constructor que recibe todos los parámetros de  Empleado  y los Inicializa
     * @param nombre
     * @param cedula
     * @param compu
     */
    public Empleado(String nombre, String cedula, Computadora compu) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.compu = compu;
    }

    /**
     * Constructor vacio para la clase Empleado
     */
    public Empleado() {

    }

    /**
     * metodo utilizado para obtener el valor de la variable privada nombre de la clase empleado
     * @return la variable de retorno simboliza el nombre del empleado
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo utilizado para modificar el atributo privado nombre de la clase Empleado
     * @param nombre Variable que simboliza el nombre de la clase empleado
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada cedula de la clase Empleado
     * @return la variable de retorno simboliza la cedula del empleado
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * Metodo utilizado para modificar el atributo privado cedula de la clase Empleado
     * @param cedula Variable que simboliza la cedula de la clase Empleado
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada compu de la clase Empleado
     * @return la variable de retorno simboliza el objeto compu
     */
    public Computadora getCompu() {
        return compu;
    }

    /**
     * Metodo utilizado para modificar el atributo privado compu de la clase
     * @param compu Variable que simboliza la compu de la clase Empleado
     */
    public void setCompu(Computadora compu) {
        this.compu = compu;
    }

     /**
          * Metodo utilizado para imprimir todos los atributos de la clase - en un unico String
          * @return la variable de retorno simboliza todos los valores de la clase - en unico string
          */
    public String toString() {
        return "Empleado{" +
                "nombre='" + nombre + '\'' +
                ", cedula='" + cedula + '\'' +
                '}';
    }
}
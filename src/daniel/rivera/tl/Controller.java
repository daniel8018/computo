package daniel.rivera.tl;

import daniel.rivera.bl.Computadora;
import daniel.rivera.bl.CapaLogica;
import daniel.rivera.bl.Empleado;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Controller {

    private CapaLogica logica = new CapaLogica();

    public void registrarComputadora(String numSerie, String marca) {
        Computadora pc = new Computadora(numSerie, marca);
        logica.registrarComputadora(pc);
    }

    public int buscarComputadora(String numSerie) {
        int encontrada = logica.buscarComputadora(numSerie);
        return encontrada;
    }

    public String[] listarComputadoras() {

        return logica.listarComputadoras();
    }

    public String[] listarPc() {

        return logica.listarPc();
    }


    public void registrarEmpleado(String cedula, String nombre) {
        Empleado emplea = new Empleado(cedula, nombre);
        logica.registrarEmpleado(emplea);
    }

    public int buscarEmpleado(String cedula) {
        int encontrada = logica.buscarEmpleados(cedula);
        return encontrada;
    }

    public String[] listarEmpleados() {
        return logica.listarEmpleados();
    }

    public boolean asociarEmpleadoComputadora(String serie, String cedula) {
        int posComputadora = logica.buscarComputadora(serie);
        if (posComputadora >= 0) {
            int posEmpleado = logica.buscarEmpleados(cedula);
            if (posEmpleado >= 0) {
                logica.asociarEmpleadoComputadora(posComputadora, posEmpleado);
                return true;
            }
        }
        return false;

    }//FIN ASOCIAREMPLEADO

}//FIN CONTROLLER

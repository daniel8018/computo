package daniel.rivera.dl;

import daniel.rivera.bl.Computadora;
import daniel.rivera.bl.Empleado;
import daniel.rivera.bl.Computadora;

import java.util.ArrayList;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class CapaLogica {
    private ArrayList<Empleado> empleados;
    private ArrayList<Computadora> computadoras;

    public CapaLogica() {
        empleados = new ArrayList<>();
        computadoras = new ArrayList<>();
    }


    public void registrarEmpleado(Empleado e) {
        empleados.add(e);
    }

    public void registarComputadoras(Computadora laptop) {
        computadoras.add(laptop);

    }



    public Computadora buscarComputadora(String serie) {
        for (Computadora laptop : computadoras) {
            if (serie.equals(laptop.getSerie())) {
                return laptop;
            }
        }
        return null;
    }



}// FIN CLASS.
